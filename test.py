import os
import tensorflow as tf
from tensorflow.keras.models import load_model
import cv2
import matplotlib.pyplot as plt
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '4'
model = load_model('model.h5')

model.compile(optimizer='adam',
              loss=tf.keras.losses.CategoricalCrossentropy(
                  from_logits=False, name='categorical_crossentropy'),
              metrics=['accuracy'])

class_descritions = [
    'left hand',
    'right hand',
    'left tilt',
    'right tilt',
    'left walk',
    'right walk'
]


def predictImage(path):
    img = cv2.imread(path)
    img = cv2.resize(img, (480, 480))
    img = np.reshape(img, [1, 480, 480, 3])
    classes = model.predict(img)
#     print(max(classes))
    return class_descritions[np.argmax(classes)]


cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = frame  # cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.resize(gray, (480, 480))
    gray = np.reshape(gray, [1, 480, 480, 3])
    classes = model.predict(gray)
#     print(max(classes))
    print(class_descritions[np.argmax(classes)])
    # Display the resulting frame
    # cv2.putText(gray, "Hello world!", (20, 20),
    #             cv2.FONT_HERSHEY_SIMPLEX, 1, gray, 2)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow('frame', gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

# for i in range(0, 6):
#     print('class: ' + str(i))
#     pred_true = 0
#     pred_all = 0
#     for filename in os.listdir('data/test/class_' + str(i) + '/'):
#         pred_all = pred_all + 1
#         arr = predictImage("data/test/class_" + str(i) + "/"+filename)
#         if arr[0][i] >= 1:
#             pred_true += 1
#     print(str(round(pred_true / pred_all * 100, 2)) + "%")
