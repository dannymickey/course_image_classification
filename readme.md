### Установка

```sh
git clone https://dannymickey@bitbucket.org/dannymickey/course_image_classification.git
```

```sh
cd course_image_classification
```

```sh
python -m venv env
```

```sh
source env/bin/activate
```

```sh
pip install -r requirements.txt
```

### Тестирование

**Программу можно сразу тестировать, потому что в репозиории уже имеется обученная модель model.h5**
**Заработает камера на ноутбуке, и в консоль будет выводиться результат кластеризации в режиме реального времени**

```sh
source env/bin/activate
```

```sh
python test.py
```

**Во время тестирования рекомендую увеличить шрифт в консоли, для более удобного просмотра результата кластеризации в режиме реального времени.**

### Обучение модели

**Необходимо в папку data добавить содержимое папки data, которую я прислал в архиве**

```sh
python train.py
```
