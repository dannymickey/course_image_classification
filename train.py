import numpy as np
import os
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras.models import Sequential
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '4'


PATH = 'data/'

train_dir = os.path.join(PATH, 'train')
validation_dir = os.path.join(PATH, 'validation')
test_dir = os.path.join(PATH, 'test')

train_lefthand_dir = os.path.join(train_dir, 'class_0')
train_righthand_dir = os.path.join(train_dir, 'class_1')
train_lefttilt_dir = os.path.join(train_dir, 'class_2')
train_righttilt_dir = os.path.join(train_dir, 'class_3')
train_leftwalk_dir = os.path.join(train_dir, 'class_4')
train_rightwalk_dir = os.path.join(train_dir, 'class_5')

validation_lefthand_dir = os.path.join(validation_dir, 'class_0')
validation_righthand_dir = os.path.join(validation_dir, 'class_1')
validation_lefttilt_dir = os.path.join(validation_dir, 'class_2')
validation_righttilt_dir = os.path.join(validation_dir, 'class_3')
validation_leftwalk_dir = os.path.join(validation_dir, 'class_4')
validation_rightwalk_dir = os.path.join(validation_dir, 'class_5')

num_lh_tr = len(os.listdir(train_lefthand_dir))
num_rh_tr = len(os.listdir(train_righthand_dir))
num_lt_tr = len(os.listdir(train_lefttilt_dir))
num_rt_tr = len(os.listdir(train_righttilt_dir))
num_lw_tr = len(os.listdir(train_leftwalk_dir))
num_rw_tr = len(os.listdir(train_rightwalk_dir))


num_lh_val = len(os.listdir(validation_lefthand_dir))
num_rh_val = len(os.listdir(validation_righthand_dir))
num_lt_val = len(os.listdir(validation_lefttilt_dir))
num_rt_val = len(os.listdir(validation_righttilt_dir))
num_lw_val = len(os.listdir(validation_leftwalk_dir))
num_rw_val = len(os.listdir(validation_rightwalk_dir))

total_train = num_lh_tr + num_rh_tr + \
    num_lt_tr + num_rt_tr + num_lw_tr + num_rw_tr
total_val = num_lh_val + num_rh_val + num_lt_val + \
    num_rt_val + num_lw_val + num_rw_val

print('total training left hand images:', num_lh_tr)
print('total training right hand images:', num_rh_tr)
print('total training left tilt images:', num_lt_tr)
print('total training right tilt images:', num_rt_tr)
print('total training left walk images:', num_lw_tr)
print('total training right walk images:', num_rw_tr)
print("----------------------------------------------------")
print('total validation left hand images:', num_lh_val)
print('total validation right hand images:', num_rh_val)
print('total validation left tilt images:', num_lt_val)
print('total validation right tilt images:', num_rt_val)
print('total validation left walk images:', num_lw_val)
print('total validation rigth walk images:', num_rw_val)
print("----------------------------------------------------")
print("Total training images:", total_train)
print("Total validation images:", total_val)

batch_size = 16
epochs = 10
IMG_HEIGHT = 480
IMG_WIDTH = 480

train_image_generator = ImageDataGenerator(
    rescale=1./255)  # Generator for our training data
validation_image_generator = ImageDataGenerator(
    rescale=1./255)  # Generator for our validation data
test_image_generator = ImageDataGenerator(
    rescale=1./255)  # Generator for our validation data

train_data_gen = train_image_generator.flow_from_directory(batch_size=batch_size,
                                                           directory=train_dir,
                                                           shuffle=True,
                                                           target_size=(
                                                               IMG_HEIGHT, IMG_WIDTH),
                                                           class_mode='categorical')

val_data_gen = validation_image_generator.flow_from_directory(batch_size=batch_size,
                                                              directory=validation_dir,
                                                              target_size=(
                                                                  IMG_HEIGHT, IMG_WIDTH),
                                                              class_mode='categorical')
test_data_gen = validation_image_generator.flow_from_directory(batch_size=batch_size,
                                                               directory=test_dir,
                                                               target_size=(
                                                                   IMG_HEIGHT, IMG_WIDTH),
                                                               class_mode='categorical')

sample_training_images, _ = next(train_data_gen)


model = Sequential([
    Conv2D(16, 2, padding='same', activation='relu',
           input_shape=(IMG_HEIGHT, IMG_WIDTH, 3)),
    MaxPooling2D(),
    Conv2D(16, 2, padding='same', activation='relu'),
    MaxPooling2D(),
    Conv2D(16, 2, padding='same', activation='relu'),
    MaxPooling2D(),
    Conv2D(16, 2, padding='same', activation='relu'),
    MaxPooling2D(),
    # Conv2D(16, 3, padding='same', activation='relu'),
    # MaxPooling2D(),
    #     Conv2D(32, 3, padding='same', activation='relu'),
    #     MaxPooling2D(),
    Flatten(),
    # Dense(32, activation='relu'),
    Dense(8, activation='relu'),
    Dense(8, activation='relu'),
    Dense(6, activation='softmax')
])
model.compile(optimizer='adam',
              loss=tf.keras.losses.CategoricalCrossentropy(
                  from_logits=False, name='categorical_crossentropy'),
              metrics=['accuracy'])
model.summary()

model.fit(
    train_data_gen,
    steps_per_epoch=total_train // batch_size,
    epochs=epochs,
    validation_data=val_data_gen,
    validation_steps=total_val // batch_size
)
model.evaluate(test_data_gen, batch_size=batch_size)
model.save('model.h5')
